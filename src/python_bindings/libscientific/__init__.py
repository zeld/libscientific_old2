from . import info
from . import vector
from . import matrix
from . import tensor
from . import pca
from . import pls
from . import vectlist

__version__ = info.Scientific().__version__
